def group_label(_port):
    if _port < 5:
        return "A"
    elif _port < 9:
        return "B"
    elif _port < 13:
        return "C"
    else:
        return "D"


def pair_type(_pA, _pB):
    '''
    需要设计路径，返回1
    可以直连，返回0
    '''
    _lA = group_label(_pA)
    _lB = group_label(_pB)
    s = set([_lA, _lB])
    if "A" in s and "D" in s:
        return 1
    if "B" in s and "C" in s:
        return 1
    return 0


def test_pair_type():
    assert 0 == pair_type(2, 5)
    assert 0 == pair_type(5, 2)

    assert 0 == pair_type(2, 10)
    assert 0 == pair_type(10, 2)

    assert 0 == pair_type(10, 15)
    assert 0 == pair_type(15, 10)

    assert 0 == pair_type(15, 6)
    assert 0 == pair_type(6, 15)

    assert 1 == pair_type(2, 15)
    assert 1 == pair_type(15, 2)

    assert 1 == pair_type(6, 10)
    assert 1 == pair_type(10, 6)


class PortGroup:
    line_port = {
        "A": [0, 1, 2, 3, 4],
        "B": [5, 6, 7, 8],
        "C": [9, 10, 11, 12],
        "D": [13, 14, 15, 16, 17]
    }


test_pair_type()
