class SimCar:
    def __init__(self, _o, _d, _plan):
        self.port_o = -1
        self.port_d = -1
        self.plan_road = _plan

        # 未初始化
        self.start_time = -1
        self.end_time = -1

        self.current_idx = -1
        self.remain_mile = -1

    def is_last_road(self):
        return self.current_idx + 1 == len(self.plan_road)

    pass
