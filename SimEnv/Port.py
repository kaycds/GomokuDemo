import SimEnv.PortGroup as pg
import SimEnv.RoadGroup as rg
import numpy as np


def port_coordinate(_port):
    _x = -1
    _y = -1
    if _port < 5:
        _x = _port + 1
        _y = 0
    elif _port < 9:
        _x = 0
        _y = _port - 4
    elif _port < 13:
        _x = 6
        _y = _port - 8
    elif _port < 18:
        _x = _port - 12
        _y = 5
    return _x, _y


def plan_route(_po, _pd):
    _type = pg.pair_type(_po, _pd)
    _x0, _y0 = port_coordinate(_po)
    _x1, _y1 = port_coordinate(_pd)
    _dirx = 1 if _x1 > _x0 else -1
    _diry = 1 if _y1 > _y0 else -1
    _route = []
    _dist = []
    # 侧面，直连
    if _type == 0:
        _r = []
        if _x0 == 0 or _x0 == 6:
            path1 = rg.roadgroup_ad[_y0 - 1][::_dirx]
            _r.extend(path1[0: abs(_x1 - _x0)])
            path2 = rg.roadgroup_bc[_x1 - 1][::_diry]
            _r.extend(path2[-abs(_y0 - _y1):])
        elif _y0 == 0 or _y0 == 5:
            path1 = rg.roadgroup_bc[_x0 - 1][::_diry]
            _r.extend(path1[0: abs(_y1 - _y0)])
            path2 = rg.roadgroup_ad[_y1 - 1][::_dirx]
            _r.extend(path2[-abs(_x0 - _x1):])
        _route.append(_r)
        _dist.append(1.0)
    # 对面，选路
    elif _type == 1:
        if _x0 == 0 or _x0 == 6:
            for _i in range(len(rg.roadgroup_bc)):
                _r = []
                path1 = rg.roadgroup_ad[_y0 - 1][:: _dirx][:_i + 1]
                path2 = rg.roadgroup_bc[::_dirx][_i  # 从左往右数，或者从右往左数
                        ][min(_y0, _y1):max(_y0, _y1):][:: _diry]  # [_y0:_y1]
                path3 = rg.roadgroup_ad[_y1 - 1][:: _dirx][_i + 1:]
                _r.extend(path1)
                _r.extend(path2)
                _r.extend(path3)
                _route.append(_r)
                # 8\9\10\4\15
                if 7 in _r or 8 in _r or 9 in _r or 3 in _r:
                    # if 7 in _r or 8 in _r or 9 in _r or 3 in _r or 14 in _r:
                    _dist.append(3.0)
                else:
                    _dist.append(2.0)
        elif _y0 == 0 or _y0 == 5:
            for _i in range(len(rg.roadgroup_ad)):
                _r = []
                path1 = rg.roadgroup_bc[_x0 - 1][:: _diry][:_i + 1]
                path2 = rg.roadgroup_ad[::_diry][_i  # 从左往右数，或者从右往左数
                        ][min(_x0, _x1):max(_x0, _x1):][:: _dirx]  # [_y0:_y1]
                path3 = rg.roadgroup_bc[_x1 - 1][:: _diry][_i + 1:]
                _r.extend(path1)
                _r.extend(path2)
                _r.extend(path3)
                _route.append(_r)
                if 7 in _r or 8 in _r or 9 in _r or 3 in _r:
                    # if 7 in _r or 8 in _r or 9 in _r or 3 in _r or 14 in _r:
                    _dist.append(3.0)
                else:
                    _dist.append(2.0)

    _dist = np.array(_dist)
    _dist = _dist / _dist.sum()
    # print(_route)
    # print(_dist)
    return _route, _dist


def test_plan_route():
    res = plan_route(12, 5)[0]
    assert [43, 37, 26, 15, 9, 8, 7, 6, 5] in res
    assert [43, 42, 36, 25, 14, 8, 7, 6, 5] in res
    assert [43, 42, 41, 35, 24, 13, 7, 6, 5] in res
    assert [43, 42, 41, 40, 34, 23, 12, 6, 5] in res
    assert [43, 42, 41, 40, 39, 33, 22, 11, 5] in res

    res = plan_route(5, 12)[0]
    assert [5, 11, 22, 33, 39, 40, 41, 42, 43] in res
    assert [5, 6, 12, 23, 34, 40, 41, 42, 43] in res
    assert [5, 6, 7, 13, 24, 35, 41, 42, 43] in res
    assert [5, 6, 7, 8, 14, 25, 36, 42, 43] in res
    assert [5, 6, 7, 8, 9, 15, 26, 37, 43] in res

    res = plan_route(0, 17)[0]
    assert [0, 6, 7, 8, 9, 15, 26, 37, 48] in res
    assert [0, 11, 17, 18, 19, 20, 26, 37, 48] in res
    assert [0, 11, 22, 28, 29, 30, 31, 37, 48] in res
    assert [0, 11, 22, 33, 39, 40, 41, 42, 48] in res

    res = plan_route(17, 0)[0]
    assert [48, 42, 41, 40, 39, 33, 22, 11, 0] in res
    assert [48, 37, 31, 30, 29, 28, 22, 11, 0] in res
    assert [48, 37, 26, 20, 19, 18, 17, 11, 0] in res
    assert [48, 37, 26, 15, 9, 8, 7, 6, 0] in res

    class Port:

        def __init__(self, _x=-1, _y=-1):
            self.x = _x
            self.y = _y

    pass


plan_route(17, 0)
