import math


class RoadSect:

    def __init__(self):
        self.speed = 14
        self.load = 1
        self.capacity = 320
        self.length = 1000

    def enterone(self):
        self.load += 1

    def leaveone(self):
        self.load -= 1

    def calspeed(self):
        # y=33.288-5.771*ln(x)
        self.speed = 33.288 - 5.771 * math.log(self.load)
        self.speed = max(self.speed, 1)
        self.speed = min(self.speed, 14)

# RoadNet = [RoadSect() for _ in range(49)]
