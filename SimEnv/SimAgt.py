import pickle
from SimEnv.SimCar import SimCar
import SimEnv.Port as pt
from SimEnv.RoadSect import RoadSect
import SimEnv.Grid as grid
import random
import numpy as np


class SimAgent():
    def __init__(self):
        self.roadnet = [RoadSect() for _ in range(49)]
        self.runplan = {}
        with open("./carplan.pkl", "rb") as f:
            self.runplan = pickle.load(f)

        self.running = []
        self.runout = []
        self.clock = 0

        self.grid_road = grid.grid_road
        self.grid_weight = grid.grid_weight

    def update_car(self, _car: SimCar):
        # 更新车辆里程：1、不换路段 2、换了路段 3、行驶完成
        # 1、行驶
        _roadidx = _car.plan_road[_car.current_idx]
        _speed = self.roadnet[_roadidx].speed
        _car.remain_mile -= _speed * 10
        # 2、换路段
        if _car.remain_mile < 0 and not _car.is_last_road():
            # 更换路段
            _t = _car.remain_mile / _speed * -1
            self.roadnet[_roadidx].leaveone()
            _car.current_idx += 1
            _roadidx = _car.plan_road[_car.current_idx]
            self.roadnet[_roadidx].enterone()
            _car.remain_mile = self.roadnet[_roadidx].length
            # 更新里程
            _speed = self.roadnet[_roadidx].speed
            _car.remain_mile -= _speed * _t
        elif _car.remain_mile < 0 and _car.is_last_road():
            self.roadnet[_roadidx].leaveone()
            _car.end_time = self.clock
            return False
        return True

    def tenseconds(self, _guidance=[]):
        # 1、更新车辆
        new_running = []
        for _car in self.running:
            if self.update_car(_car):
                new_running.append(_car)
            else:
                self.runout.append(_car)
        self.running = new_running
        # 2、新车修改路径
        _modify = []  # 存储可修改车辆下标
        _control_road = set()
        _newcar = self.runplan.get(self.clock, [])
        # 取得控制范围
        for _gidx in _guidance:
            _r = self.grid_road[_gidx]
            _control_road = _control_road | set(_r)
        # 统计，如果在范围内，且可以实现避让。
        for _i in range(len(_newcar)):
            _car = _newcar[_i]
            if len(set(_car.plan_road) & _control_road) > 0:  # 进入管制区域
                _routes, _weights = pt.plan_route(_car.port_o, _car.port_d)
                for _j in range(len(_routes)):
                    if len(set(_routes[_j]) & _control_road) == 0:
                        _modify.append(_i)
                        break
        # 取30%的车辆进行避让
        # if len(_guidance) != 0:
        #     print("modify:", len(_modify) * 0.3)
        for _i in range(int(len(_modify) * 0.2)):
            _availpath = []
            _car = _newcar[_modify[_i]]
            _routes, _weights = pt.plan_route(_car.port_o, _car.port_d)
            for _r in _routes:
                if len(set(_r) & _control_road) == 0:
                    _availpath.append(_r)
            _car.plan_road = random.choice(_availpath)

            pass

        # 3、加入车辆
        for _car in _newcar:
            _car.start_time = self.clock
            _car.current_idx = 0
            _roadidx = _car.plan_road[_car.current_idx]
            _car.remain_mile = self.roadnet[_roadidx].length
            self.running.append(_car)
            self.roadnet[_roadidx].enterone()
        # 4、更新路网速度
        for _r in self.roadnet:
            _r.calspeed()
        self.clock += 1
        pass

    def eval_total(self):
        _total = 0
        for _car in self.runout:
            _total += _car.end_time - _car.start_time
        print("total(hour)", _total / 360)
        # 3460.53888888888

    def print_road_state(self, _i):
        road_speed = ["%.2f" % r.speed for r in self.roadnet]
        _acc = 0
        print(_i)
        for _j in [5, 6, 5, 6, 5, 6, 5, 6, 5]:
            pt = '\t'.join(road_speed[_acc:_acc + _j])
            _acc += _j
            print(pt)
        print()

    def situationa(self, _old_sit):
        # 速度，负载量，管制状态
        _speed = np.zeros((9, 1))
        _load = np.zeros((9, 1))
        _guid = _old_sit[:, :, -1][:]

        for _i in range(len(self.grid_road)):
            _sp = np.array([self.roadnet[_j].speed for _j in self.grid_road[_i]])
            _speed[_i, 0] = int(np.sum(_sp * self.grid_weight[_i]) / np.sum(self.grid_weight(_i)))

            _ld = np.array([self.roadnet[_j].load for _j in self.grid_road[_i]])
            _load[_i, 0] = int(np.sum(_ld * self.grid_weight[_i]) / np.sum(self.grid_weight(_i)) / 20)
        _new_sit = np.concatenate((_speed, _load, _guid), axis=1).reshape((3, 3, 3))
        _ret_sit = np.concatenate((_old_sit, _new_sit), axis=2)[:, :, -3:]
        return _ret_sit

    def run(self, _old_sit=np.zeros((3, 3, 3))):
        for _i in range(24):
            # print(len(self.running), len(self.runout))
            # self.print_road_state(_i)

            _guid = []
            # if _i > 2 and _i < 7:
            #     _guid = [4]
            #     _old_sit[:, :, -1][_guid] = 1
            # else:
            #     _old_sit[:, :, -1][:] = 0

            # _con = _old_sit[:, :, -1].reshape((-1, 1))
            # _guid = np.nonzero(_con)[0].tolist()
            for _j in range(30):  # 30*10=5min
                self.tenseconds(_guid)
            # _old_sit = self.situationa(_old_sit=_old_sit)
        self.eval_total()


sa = SimAgent()
sa.run()
