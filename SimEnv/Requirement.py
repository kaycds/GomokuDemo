import numpy as np
import SimEnv.SimCar as sc
import SimEnv.Port as port
from SimEnv.SimCar import SimCar
from SimEnv.PortGroup import PortGroup
import random
import pickle


class Requirement:

    def __init__(self):
        self.time_len = 540  # 180用于平复
        self.rate_arr = [0.2, 0.2, 0.2, 0.2, 0.2]  # 比例
        self.rate_weight = [0.1, 0.3, 0.2, 0.1, 0.0]  # 速率
        factor = sum([r * w for r, w in zip(self.rate_arr, self.rate_weight)])
        self.total_car = factor * self.time_len * 10 * 18
        print(self.total_car)
        self.od_weight = np.array([1, 1, 1, 1 + 0.1, 1, 1 + 0.1,
                                   1, 1, 1, 1 + 0.1, 1, 1,
                                   1, 1, 1, 1, 1, 1])  # 4,6,10,17
        self.od_weight = self.od_weight.reshape(18, 1)
        self.od_weight = self.od_weight / self.od_weight.sum()
        self.od_mat = self.od_weight * self.od_weight.T
        self.cut_od_mat()
        self.od_mat = self.total_car * self.od_mat
        self.od_mat = self.od_mat.astype(int)
        self.car_mat = np.array([None] * 18 * 18).reshape((18, 18))
        self.init_car_mat()
        self.car_plan = {}
        self.gen_car_plan()

    def cut_od_mat(self):
        # 1、删除相邻网格
        # 网格-入口
        grid_port = [[0, 1, 5],  # 1
                     [2, 3],
                     [4, 9],  # 3
                     [6],
                     [],  # 5
                     [10],
                     [7, 8, 13, 14],  # 7
                     [15, 16],
                     [11, 12, 17]  # 9
                     ]
        # 网格-相邻网格
        grid_pair = {0: [0, 1, 3],
                     1: [0, 1, 2],
                     2: [1, 2, 5],
                     3: [0, 3, 6],
                     4: [],
                     5: [2, 5, 8],
                     6: [3, 6, 7],
                     7: [6, 7, 8],
                     8: [5, 7, 8]
                     }
        # 遍历所有网格
        for grid_i in range(9):
            source_port = grid_port[grid_i]  # 原入口
            target_grid = grid_pair[grid_i]  # 相邻网格
            target_port = set()
            # 相邻网格包含哪些入口
            for t in target_grid:
                port = grid_port[t]
                target_port |= set(port)
            target_port = list(target_port)
            for s in source_port:
                self.od_mat[[s] * len(target_port), target_port] = 0

        # 2、删除同线入口
        for k, vs in PortGroup.line_port.items():
            for v in vs:
                self.od_mat[[v] * len(vs), vs] = 0
        self.od_mat = self.od_mat / self.od_mat.sum()

    def init_car_mat(self):
        # 生成车辆，按照比例分配路径
        newsum = 0
        for _i in range(18):
            for _j in range(18):
                self.car_mat[_i, _j] = list()
                if self.od_mat[_i, _j] == 0:
                    continue
                _routes, _weights = port.plan_route(_po=_i, _pd=_j)
                _dist_num = self.od_mat[_i, _j] * _weights
                _dist_num = _dist_num.astype(int)
                # newsum += _dist_num.sum()
                # print(len(_weights))
                for _k in range(len(_weights)):
                    self.car_mat[_i, _j].extend([SimCar(_o=_i, _d=_j, _plan=_routes[_k])
                                                 for _ in range(_dist_num[_k])])
                # print(len(self.car_mat[_i, _j]))
                random.shuffle(self.car_mat[_i, _j])
                newsum += len(self.car_mat[_i, _j])
        print("newsum", newsum, "按路径比例，生成车辆，分路径")

    def gen_car_plan(self):
        # 生成出行计划
        _car_queue = []
        # 30s一个区间，加进去然后打散
        _left = np.zeros((18, 18)).astype(int)
        for _i in range(1, 181):
            _shuffle_queue = []
            for _j in range(18):
                for _k in range(18):
                    if self.od_mat[_j, _k] == 0:
                        continue
                    # print(len(self.car_mat[_j, _k]))
                    _l = _left[_j, _k]
                    _r = int(len(self.car_mat[_j, _k]) * _i / 180)
                    # print(_l, _r)
                    new_queue = self.car_mat[_j, _k][_l: _r]
                    if len(new_queue) != 0:
                        _shuffle_queue.extend(new_queue)
                        _left[_j, _k] = _r
            random.shuffle(_shuffle_queue)
            # newsum += len(_shuffle_queue)
            _car_queue.extend(_shuffle_queue)
        print("newsum", len(_car_queue), "各个网格，按照30s一次，组合生成出车队列")

        # 生成出行时间表，时间分成5段，每段108个时间片，需求量分别是1，2，4，2，1
        # self.rate_arr = [0.2, 0.2, 0.2, 0.2, 0.2]
        # self.rate_weight = [0.2, 0.4, 0.8, 0.4, 0.2]
        newsum = 0
        _dist = np.array(self.rate_weight)
        _dist = _dist / _dist.sum()
        _distnum = len(_car_queue) * _dist
        _l = 0
        _accumlate = 0
        for _i in range(5):
            for _j in range(1, 109):
                _r = int(_distnum[_i] / 108 * _j + _accumlate)
                if _r - _l > 0:
                    newsum += _r - _l
                    self.car_plan[108 * _i + _j - 1] = _car_queue[_l: _r]
                    _l = _r
            _accumlate += _distnum[_i]
        print("newsum", newsum, "按照流量比例，生成时间表")
        # for _i in range(540):
        #     print(len(self.car_plan[_i]))

        with open("./carplan.pkl", "wb") as f:
            pickle.dump(self.car_plan, f)


r = Requirement()

# print(r.od_mat.tolist())
# x = np.array(range(9)).reshape(3, 3)
# print(x[[0, 1, 2], [1, 1, 1]])
# print(PortGroup.line_port)
